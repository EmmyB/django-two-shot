from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account
from django import forms


class CreateForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]
        widgets = {
            "date": forms.DateInput(attrs={"type": "date"}),
        }

class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]



class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]